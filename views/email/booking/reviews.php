<style>
    .review_item
    {
        width: 80%;
        display: inline-block;
    }
    .review_data{
        width: 10%;
        display: inline-block;
    }
    .review_text{
        width: 60%;
        display: inline-block;
        vertical-align: top;
        margin: 5px;
        border: 1px solid #000;
        //height: auto;
    }
    .review_data_item{
        text-align: center;
        border: 1px solid #000;
        margin: 5px;
        //display: inline-block;
    }
</style>

<h1>Booking New Reviews</h1>
<?php
foreach ($reviews as $review) {
    ?>   
    <div class="review_item">
        <div class="review_data">
            <div class="percentage review_data_item">Percent: <?php echo $review['percentage'] ?></div>
            <div class="nights review_data_item">Nights: <?php echo $review['nights'] ?></div>
            <div class="room review_data_item">Room Type: <?php echo $review['room'] ?></div>
        </div>
        <div class="review_text"><?php echo!empty($review['text']) ? $review['text'] : "There are no comments available for this review" ?></div>
    </div>
    <?php
}
?>
