<ul class="nav nav-tabs main-tabs">
	<li class="active"><a href="#Properties">Properties</a></li>
	<li class=""><a href="#Users">Users</a></li>
</ul>
<div class="tab-content main-tab-content">
    <div class="tab-pane active" id="Properties"></div>
	<div class="tab-pane" id="Users"></div>
</div>
<script>
	$( document ).ready( function() {
		$( '.main-tabs a' ).click( function( e ) {
			e.preventDefault();
			var action = $( this ).attr( 'href' ).substr( 1 );
			var that = this
			$( '.main-tab-content div' ).empty();
			$( this ).tab( 'show' )
			$.post( '?act=' + action, '', function( data ) {
				$( '.main-tab-content div.active' ).html( data )
			}, 'HTML' )
		} )
		$( '.main-tabs .active a' ).click();

	} )
</script>