<select class="select2 js-basic-multiple-<?php echo $group_id ?>" multiple="multiple" style="width: 100%;">
	<?php foreach ( $properties as $property ) {
		?>
	<option value="<?php echo $property[ 'id' ] ?>" selected><?php echo $property[ 'name' ]?></option>
			<?php
		}
		?>
</select>
<script>

    $( ".js-basic-multiple-<?php echo $group_id ?>" ).select2( {
        ajax: {
            url: "?act=GetProperties",
            dataType: 'json',
            delay: 250,
            data: function ( params ) {
                return {
                    property: params.term, // search term
                    page: params.page
                };
            },
            processResults: function ( data, page ) {
				console.log(data)
                return {
                    results: data
                };
            },
            cache: true
        },
        escapeMarkup: function ( markup ) {
            return markup;
        }, // let our custom formatter work
        templateResult: formatItem, // omitted for brevity, see the source of this page
        templateSelection: formatItemSelection // omitted for brevity, see the source of this page
    } );

    function formatItem( data ) {
        if ( data.loading )
            return data.text;
//		console.log(data)
        return  '<div>' + data.text + '</div>';
    }

    function formatItemSelection( data ) {
//		console.log(data)
        return data.text;
    }
</script>