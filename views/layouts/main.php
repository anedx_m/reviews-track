<html>
    <head>
        <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.theme.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
		<link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/select2.min.css">
		<script type="text/javascript" src="assets/js/select2.full.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
		<script type="text/javascript" src="assets/js/Table.js"></script>
		<script type="text/javascript" src="assets/js/afterEvents.js"></script>
		<title>Reviews Tracking Admin</title>
    </head>    
    <body>
		<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">Are you sure?</div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete-confirm">Delete</button>
						<button type="button" data-dismiss="modal" class="btn">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<script>
			(function($) {
				$.fn.confirmDeleteDialog = function(question, callback) {
					if (!question)
						question = 'Are you sure?';
					$('#modalConfirm .modal-body').html(question);
					
					$('#modalConfirm').addClass( "ui-corner-z-index" );
					
					$('#modalConfirm').unbind().modal();
					$('#modalConfirm #delete-confirm').unbind().on('click', function() {
						callback();
					});
				}
			}(jQuery));
		</script>
    
		<?php
		$this->renderPartial( $view, $param );
		?>
    </body>
</html>



