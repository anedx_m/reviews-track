<input type="hidden" value="<?php echo $order_by ?>" class="order_by">
<input type="hidden" value="<?php echo $direction ?>" class="direction">

<?php
if ( $pagination[ 'amount_pages' ] > 0 ):
	?>
	<div class="pages_info">
		<span><?php echo $start_el ?> - <?php echo $end_el ?> of <?php echo $total ?></span>
	</div>
<?php endif; ?>
<table class="table table-hover table-striped">
	<tr class="table-header">
		<?php
		foreach ( $fields as $field => $alias ):
			$sortable = in_array( $field, $sortables ) ? 'sortable' : '';
			?>
			<th data-order_by="<?php echo $field ?>" class="<?php
			echo $order_by == $field ? $direction : '';
			echo ' ' . $sortable
			?>"><?php echo $alias ?></th>
				<?php
			endforeach;
			?>
	</tr>
	<?php
	foreach ( $data as $data_row ) {
		$row = $Table->processingRow( $data_row );
		echo empty( $row ) ? '<tr>' : $row;
		foreach ( $fields as $field => $alias ):
			?>
			<td class="<?php echo $field ?>"><?php
				echo $Table->processingField( $data_row, $field );
				?></td>
			<?php
		endforeach;
		?>
	</tr>
	<?php
}
?>
</table>
<?php
if ( $pagination[ 'amount_pages' ] > 0 ):
	?>
	<div class="pages_info">
		<span><?php echo $start_el ?> - <?php echo $end_el ?> of <?php echo $total ?></span>
	</div>
	<div class="t-pagination">
		<?php
		if ( $pagination[ 'start_page' ] > 1 ):
			?>
			<div class="prev number-page" data-page="<?php echo $pagination[ 'start_page' ] - 1 ?>"><i class="fa fa-angle-double-left"></i></div>
			<?php
		endif;
		for ( $i = $pagination[ 'start_page' ]; $i <= $pagination[ 'end_page' ]; $i++ ):
			if ( $i == $pagination[ 'current_page' ] ):
				?>
				<div class="number-page current-page" data-page="<?php echo $i ?>"><?php echo $i ?></div>
				<?php
			else :
				?>
				<div class="number-page" data-page="<?php echo $i ?>"><?php echo $i ?></div>
			<?php
			endif;
		endfor;
		if ( $pagination[ 'end_page' ] < $pagination[ 'amount_pages' ] ):
			?>
			<div class="next number-page" data-page="<?php echo $pagination[ 'end_page' ] + 1 ?>"><i class="fa fa-angle-double-right"></i></div>
			<?php
		endif;
		?>
	</div>
	<div class="clearfix"></div>
	<?php
endif;
?>
