<div class="padding-10">
    <input type="button" class="btn btn-default" value="Add new property" id="add_new_property">
</div>
<div style="display: none;" id="property">
    <form class="property_form">
        <input type="hidden" name="property_id" id="property_id">
        <label style='width:100%'>Property name: <input type="text" name="property_name" id="property_name" style='width:60%'></label>
        <label style='width:100%' >Hostelworld Review Link: <input type="text" name="hostelworld_url" id="hostelworld_url"  style='width:60%'></label>
        <label  style='width:100%'>Booking.com Review Link: <input type="text" name="booking_url" id="booking_url"  style='width:60%'></label>
    </form>
</div>
<div id="list_properties" class="simple-table"></div>
<script>

    var t_properties = new Table('?act=ListProperties', '#list_properties');
    t_properties.list( )


    $('#add_new_property').click(function() {
        var pr_form = $('#property').clone().find('.property_form');
        ;
        pr_form.find('#property_id').val('');
        $(pr_form)[0].reset();
        bootbox.dialog({
			onEscape: function() {bootbox.hideAll()},
            message: pr_form,
            title: "Add New Property",
            buttons: {
                save: {
                    label: "Save",
                    className: "btn-success",
                    callback: function() {
                        $.post('?act=SaveProperty', $('.bootbox .property_form').serialize(), function(data) {
                            t_properties.list( )
                        })
                    }
                },
            }
        });
    })
    t_properties.afterList.add(function() {

        $('.delete_property').click(function() {
            var property_id = $(this).attr('data-id');
            $(this).confirmDeleteDialog("Delete Property?", function() {
                $.post('?act=DeleteProperty', 'property_id=' + property_id, function(data) {
                    t_properties.list( )
                })
            });
        })

        $('.edit_property').click(function() {
            var property_id = $(this).attr('data-id');

            var pr_form = $('#property').clone().find('.property_form');

            $(pr_form)[0].reset();
            $.post('?act=GetProperty', 'property_id=' + property_id, function(data) {
                console.log(data)
                data = data.msg;
                pr_form.find('#property_id').val(property_id);
                pr_form.find('#property_name').val(data.name);
                pr_form.find('#hostelworld_url').val(data.hostelworld_url);
                pr_form.find('#booking_url').val(data.booking_url);

                bootbox.dialog({
					onEscape: function() {bootbox.hideAll()},
                    message: pr_form,
                    title: "Edit Property",
                    buttons: {
                        save: {
                            label: "Save",
                            className: "btn-success",
                            callback: function() {
                                $.post('?act=SaveProperty', $('.bootbox .property_form').serialize(), function(data2) {
                                    t_properties.list( )
                                })
                            }
                        },
                    }
                });
            }, 'JSON')

        })

        $('.parse_property').click(function() {
            var win = window.open('cron/property_parser.php?id=' + $(this).attr('data-id'), '_blank');
            win.focus();
//            $.ajax({
//                url: 'cron/property_parser.php',
//                dataType: "json",
//                data: {
//                    id: $(this).attr('data-id') ,
//                },
//                success: function(data) {
//
//                },
//            });
        });
    })

</script>

