<div class="padding-10">
	<input type="button" class="btn btn-default" value="Add new user" id="add_new_user">
</div>
<div style="display: none;" id="user">
	<form class="user_form">
		<input type="hidden" name="user_id" id="user_id">
		<label>User Nickname: <input type="text" name="user_name" id="user_name"></label>
		<label>User Email: <input type="text" name="user_email" id="user_email"></label>
	</form>
</div>
<div id="list_users" class="simple-table"></div>

<script>

    var t_users = new Table( '?act=ListUsers', '#list_users' );
    t_users.list( )


    $( '#add_new_user' ).click( function () {
        var pr_form = $( '#user' ).clone().find( '.user_form' );
        ;
        pr_form.find( '#user_id' ).val( '' );
        $( pr_form )[0].reset();
        bootbox.dialog( {
			onEscape: function() {bootbox.hideAll()},
            message: pr_form,
            title: "Add New User",
            buttons: {
                save: {
                    label: "Save",
                    className: "btn-success",
                    callback: function () {
                        $.post( '?act=SaveUser', $( '.bootbox .user_form' ).serialize(), function ( data ) {
                            t_users.list( )
                        } )
                    }
                },
            }
        } );
    } )
    t_users.afterList.add( function () {
    
        $( '.delete_user' ).click( function () {
			var user_id = $( this ).attr( 'data-id' );
			$(this).confirmDeleteDialog("Delete User?", function () {
				$.post( '?act=DeleteUser', 'user_id=' + user_id, function ( data ) {
					t_users.list( )
				} )
			});
        } )
        
        $( '.edit_user' ).click( function () {
            var user_id = $( this ).attr( 'data-id' );

            var pr_form = $( '#user' ).clone().find( '.user_form' );

            $( pr_form )[0].reset();
            $.post( '?act=GetUser', 'user_id=' + user_id, function ( data ) {

                data = data.msg;
                pr_form.find( '#user_id' ).val( user_id );
                pr_form.find( '#user_name' ).val( data.name );
                pr_form.find( '#user_email' ).val( data.email );

                bootbox.dialog( {
					onEscape: function() {bootbox.hideAll()},
                    message: pr_form,
                    title: "Edit User",
                    buttons: {
                        save: {
                            label: "Save",
                            className: "btn-success",
                            callback: function () {
                                $.post( '?act=SaveUser', $( '.bootbox .user_form' ).serialize(), function ( data2 ) {
                                    t_users.list( )
                                } )
                            }
                        },
                    }
                } );
            }, 'JSON' )

        } )
        $( '.frequency_reports' ).click( function () {
            var user_id = $( this ).attr( 'data-id' );
            $.post( '?act=EditFrequencyReport&user_id=' + user_id, '', function ( data ) {
                bootbox.dialog( {
					onEscape: function() {bootbox.hideAll()},
                    message: data,
                    title: "Frequency of Reports",
                    buttons: {
                        close: {
                            label: "Close",
                            className: "btn-success",
                        },
                    },
					className: 'edit-frequency-reports'
                } );
            } )
        } )
    } )


    t_users.afterList.add( function () {
        $( '.assign_group' ).click( function () {
            var user_id = $( this ).attr( 'data-id' );
            $.post( '?act=EditGroup&user_id=' + user_id, '', function ( data ) {
                bootbox.dialog( {
					onEscape: function() {bootbox.hideAll()},
                    message: data,
                    title: "Groups",
                    buttons: {
                        close: {
                            label: "Close",
                            className: "btn-success",
                        },
                    },
					className: 'edit-group'
                } );
            } )
        } )
    } )
</script>

