<div class="padding-10">
	<label>type new Group: </label> <input id="group_name" style="width: 50%; padding: 5px;" ><input type="button" id="add_group" class="hide btn btn-default" value="Add Group" >
</div>

<div id="list_user_grooups" class="simple-table"></div>

<script>
	
    var user_id = '<?php echo $user_id ?>'

    var t_groups = new Table( '?act=ListUserGroups', '#list_user_grooups' );
    t_groups.list( { user_id: user_id } )

    t_groups.afterList.add( function () {
		
        $( '.delete_group' ).click( function () {
			var group_id = $( this ).attr( 'data-id' );
			$(this).confirmDeleteDialog("Delete Group?", function () {
				$.post( "?act=DeleteGroup&user_id=" + user_id, 'group_id=' + group_id, function ( data ) {
					t_groups.list( { user_id: user_id } )
				} )
			});
        } )
    } )
    
    t_groups.afterList.add( function () {
        $( ".select2" ).change( function () {
            var group_id = $( this ).closest( 'tr' ).attr( 'data-id' )
            var property_ids = { }
            $( this ).find( "option:selected" ).each( function ( i, selected ) {
                property_ids[i] = $( selected ).val();
            } );
            $.post( '?act=SaveGroup&group_id=' + group_id + '&user_id=' + user_id, { property_ids: property_ids }, function ( data ) {

            } )
        } )

    } )

    $( "#group_name" ).keydown( function ( e ) {
        $( this ).addClass( 'preloader' )
        $( '#add_group' ).addClass( 'hide' )
    } )
    $( "#group_name" ).afterKeyDown( function ( e ) {
//		console.log(e)
        var group_name = $( e.currentTarget ).val();
        if ( group_name.length == 0 ) {
            return;
        }
        $.post( "?act=GetGroup&user_id=" + user_id, 'group_name=' + group_name, function ( data ) {
            $( "#group_name" ).removeClass( 'preloader' )
//            console.log( data )
            if ( data == "false" ) {
                $( '#add_group' ).removeClass( 'hide' )
            }
        } )
    }, 200, true )

    $( "#group_name" ).autocomplete( {
        source: function ( request, response ) {
            $.ajax( {
                url: "?act=GetGroups&user_id=" + user_id,
                dataType: "json",
                data: {
                    group_name: request.term
                },
                success: function ( data ) {
                    response( data );
                }
            } );
        },
        minLength: 1,
        select: function ( event, ui ) {
			$( '#add_group' ).addClass( 'hide' )
            return this.value;
        },
        open: function () {
            $( '.ui-autocomplete' ).addClass( "ui-corner-z-index" );
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

        },
        close: function () {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    } );
    $( '#add_group' ).click( function () {
        var group_name = $( '#group_name' ).val();
        var properties = '';
        $.post( '?act=SaveGroup', 'group_name=' + group_name + '&user_id=' + user_id + '&properties=' + properties, function () {
            t_groups.list( { user_id: user_id } )
            $( '#add_group' ).addClass( 'hide' )
        } )
    } )
</script>