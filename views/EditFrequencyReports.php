<div>
    <div class="padding-10">
        <div class="padding-10">
            <label>type Property or Group to add: </label> <input type="text" id="entity_name" style="width: 100%; padding: 5px;">
        </div>
        <div class="hide add_new_frequency" id="add">
            <form id="selected_entity">
                <div class="row row_new_frequency" style="margin:5px;">
                    <div class="col-lg-5">
                        <span id="selected_entity_name" style="font-size: 20px;"></span>
                    </div>
                    <div class="col-lg-4">
                        <input type="hidden" id="selected_entity_type" name="entity_type">
                        <input type="hidden" id="selected_entity_id" name="entity_id">
                        <label><input class="frequency-radio" type="radio" name="frequency_new" value="1">Daily</label>&nbsp;
                        <label><input class="frequency-radio" type="radio" name="frequency_new" value="2">Bi-Monthly</label>&nbsp;
                        <label><input class="frequency-radio" type="radio" name="frequency_new" value="3">Both</label>&nbsp;
                    </div>
                    <div class="col-lg-3">
						<input type="button" id="hide_fr" class="btn btn-default" value="Cancel" style="float: right;" >
						<span style="float: right;">&nbsp;</span>
                        <input type="button" id="add_btn" class="btn btn-primary" value="Add" style="float: right;" >
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<div id="list_frequency_reports" class="simple-table"></div>

<script>

    var user_id = '<?php echo $user_id ?>'

    var t_freports = new Table('?act=ListFrequencyReports', '#list_frequency_reports');
    t_freports.list({user_id: user_id})

    t_freports.afterList.add(function() {

        $('.delete_frequency_report').click(function() {
            var frequency_report_id = $(this).attr('data-id');
            $(this).confirmDeleteDialog("Delete Report?", function() {
                $.post("?act=DeleteFrequencyReport&frequency_report_id=" + frequency_report_id, '', function(data) {
                    t_freports.list({user_id: user_id})
                })
            });
        })

        $('.view_report').click(function() {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            var stat_type = $(this).parent().parent().find("input:checked").val();
            var win = window.open('cron/sample_report.php?id=' + id + '&stat_type=' + stat_type+ '&type=' + type, '_blank');
            win.focus();
        })

        $('.frequency-radio').click(function() {
            var frequency_report_id = $(this).closest('tr').attr('data-id');
            var frequency = $(this).val();
            console.log(frequency)
            $.post("?act=SaveFrequencyReport&frequency_report_id=" + frequency_report_id, 'frequency=' + frequency, function(data) {
//                t_freports.list( { user_id: user_id } )
            })
        })
    })
    $("#entity_name").keydown(function(e) {
        $(this).addClass('preloader')
        $('#add').addClass('hide')
    })
    $("#entity_name").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "?act=GetEntityNames&user_id=" + user_id,
                dataType: "json",
                data: {
                    entity_name: request.term
                },
                success: function(data) {
                    $("#entity_name").removeClass('preloader')
                    response($.map(data, function(item) {
                        var pg = '(G) ';
                        if (item.entity_type == 'property')
                            pg = ' ';
//                        console.log( item )
                        return {label: pg + item.name, value: item.entity_id, type: item.entity_type}
                    }))
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $('#add #selected_entity_name').removeClass();

            $('#add #selected_entity_name').html(ui.item.label).addClass(ui.item.type);
            $('#add').removeClass('hide')
            $("#entity_name").val('')
            $("#selected_entity_type").val(ui.item.type)
            $("#selected_entity_id").val(ui.item.value)
//            console.log( ui )

            return false;
        },
        open: function() {
            $('.ui-autocomplete').addClass("ui-corner-z-index");
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");

        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
    $("#entity_name").attr('autocomplete', 'off');

    $('#add_btn').click(function() {
        $.post('?act=AddFrequencyReport&user_id=' + user_id, $('#selected_entity').serialize(), function() {
            t_freports.list({user_id: user_id})
            $('#add').addClass('hide')
            $("#selected_entity")[0].reset();
        }
        )
    })
    $('#hide_fr').click(function() {
        $('#add').addClass('hide')
    })


</script>