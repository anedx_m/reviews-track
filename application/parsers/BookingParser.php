<?php

namespace parsers;

class BookingParser extends AbstractHotelParser {

    var $table = 'booking_reviews';

    function parse_hotel($url, $property_id) {
        require_once(dirname(__FILE__) . '/../../includes/simple_html_dom.php');
        $data = array();
        $ids = array();
        if (preg_match('#\/(..)\/(.*?).html#s', $url, $m)){
            $name = $m[2];
            $cc=$m[1];
        }
        else // debug error?
            return;
        
        $counts_from = 0;
        do {
            $url = "http://www.booking.com/reviewlist.en-us.html?pagename=$name;cc1=$cc;type=total;dist=1;offset=$counts_from;rows=100;r_lang=en;sort=f_recent_desc;rid=&_=1438084430564";
            echo $url;
            $page = $this->getCachedUrl($url);
            $html = str_get_html($page);
            //die($page);
            $counts_block = $html->find('.page_showing', 0);
            if (isset($counts_block)) {
                $counts_block = preg_replace('/[^\d-]+/', '', $counts_block->innertext);
                //echo "\n".$counts_block;
                $buf = explode('-', $counts_block);
                $counts_from = $buf[1];
                $count = $buf[0];
            } else
                break;

            $reviews = $html->find('.review_item');
            if (!empty($reviews)) {
                foreach ($reviews as $item) {
					if(empty($item))
						continue;
					echo "-----";
                    var_dump($item->innertext);
					echo "++++";
                    $reviewData = array();

                    $reviewData['property_id'] = $property_id;
                    $reviewData['status'] = 'processing';

                    $review_id_block = $item->find('[name=object_id]', 0);
                    if (!isset($review_id_block))
                        continue;
                    $reviewData['review_id'] = $review_id_block->value;
                    var_dump($reviewData['review_id']);

                    $review_date_block = $item->find('.review_item_date', 0);
                    if (!isset($review_date_block))
                        continue;
                    $reviewData['date'] = date("Y-m-d", strtotime($review_date_block->innertext));

                    if ($this->check_on_stop_search($reviewData)) {
                        unset($html);
                        break 2;
                    }

                    $review_score_block = $item->find('.review_item_review_score', 0);
                    if (isset($review_score_block))
                        $reviewData['percentage'] = $review_score_block->innertext * 10;
                    else
                        $reviewData['percentage'] = 0;

                    $reviewData['text']  = '';    
                    $review_text_block = $item->find('.review_neg', 0);
                    if (isset($review_text_block))
                        $reviewData['text'] = '- '. $review_text_block->innertext;
                        
                    $review_text_block = $item->find('.review_pos', 0);
                    if (isset($review_text_block))
                        $reviewData['text'] = trim( $reviewData['text'] . "\n+ ".$review_text_block->innertext);

                    $infotags = $item->find('.review_info_tag', 3);
                    if (isset($infotags))///kostil'
                        $i = 0;
                    else
                        $i = -1;
                    //review_item_info_tags
                    $review_peoples_block = $item->find('.review_info_tag', $i + 1);
                    if (isset($review_peoples_block))
                        $reviewData['peoples'] = trim(preg_replace('/<span\b[^>]*>(.*?)<\/span>/i', '', $review_peoples_block->innertext));

                    $review_room_block = $item->find('.review_info_tag', $i + 2);
                    if (isset($review_room_block))
                        $reviewData['room'] = trim(preg_replace('/<span\b[^>]*>(.*?)<\/span>/i', '', $review_room_block->innertext));

                    $review_nights_block = $item->find('.review_info_tag', $i + 3);
                    if (isset($review_nights_block))
                        $reviewData['nights'] = trim(preg_replace('/[^\d]+/i', '', $review_nights_block->innertext));

                    $commentBlock = $item->find('.translate', 0);

					//print_r($reviewData);die();
                   // $ids[] = $this->insert_review_in_db($reviewData);
                }
            }
            unset($html);
        } while ($count <= $counts_from);
        $this->complete_status_reviews($ids);
        return count($ids);
    }

}
