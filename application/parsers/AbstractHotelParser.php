<?php
namespace parsers;

abstract class AbstractHotelParser {

    var $output = 1;
    //no need for you, just ignore  next line
    var $cacheOn = 1; //debug
    var $depth_search_days = 365; // 0 if u want get reviews for all time
    var $table = '';

    function __construct() {
        $this->urlCacheFolder = dirname(__FILE__) . '/../../_cache/';
        if ($this->cacheOn) {
            $this->cookieFile = $this->urlCacheFolder . get_class($this) . '-cookies.txt';
        } else {
            $this->cookieFile = tempnam('/tmp', 'cookies');
        }
    }

    function __destruct() {
        if (!$this->cacheOn) {
            unlink($this->cookieFile);
        }
    }

    function getURL($url, $data = '', $head = 0, $follow = 1, $referer = '') {
        $ch = curl_init();
        $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Accept-Language: en-us";
        $header[] = "Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Keep-Alive: 300";
        $header[] = "Connection: keep-alive";
        $header[] = "X-Requested-With: XMLHttpRequest";
        $header[] = "Pragma: no-cache";
        $header[] = "Cache-Control: no-cache";
        $header[] = "Expect:";

        if (!empty($this->extraheaders)) {
            $header = array_merge($header, $this->extraheaders);
            $this->extraheaders = false;
        }

        if (empty($referer)) {
            $referer = $url;
        }

        curl_setopt($ch, CURLOPT_HEADER, $head);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $follow);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);


        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);


        if ($data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $d = curl_exec($ch);
        $err = curl_error($ch);


        curl_close($ch);
        if ($err) {
            throw new Exception("curl returned '$err' for $url");
        }

        return $d;
    }

    function getBody($page) {
        return preg_replace('#HTTP/\d\.\d.*?$.*?\r?\n\r?\n#ims', '', $page);
    }

    function getCachedUrl($url, $data = "") {
        global $cacheOn, $urlCacheFolder;
        //using cache?
        if (is_array($data)) {
            $hash = md5($url . $this->postData($data));
        } else {
            $hash = md5($url . $data);
        }
        $cacheFile = $this->urlCacheFolder . $hash . '.html';

        if (!$this->cacheOn) {
            $page = $this->getURL($url, $data, $head = 1, $follow = 1, $referer = '');
        } elseif (!file_exists($cacheFile)) {
            $page = $this->getURL($url, $data, $head = 1, $follow = 1, $referer = '');
            file_put_contents($cacheFile, $page);
        } else {
            $page = file_get_contents($cacheFile);
        }

        $this->last_page = $page;
        return $page;
    }

    function parse_hotel($url, $property_id) {
        
    }

    function check_on_stop_search($review) {

        $db = \models\DB::getInstance();
        
        if ($this->depth_search_days > 0)
            if ((strtotime('now') - strtotime($review['date'])) > ($this->depth_search_days * 24 * 60 * 60))
                return true;

        $sql = 'SELECT review_id FROM ' . $this->table . ' WHERE review_id = ? AND date = ? AND status <> ? LIMIT 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($review['review_id'], $review['date'], 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        if ($result)
            return true;
        else
            return false;
    }

    function insert_review_in_db($review) {
       
        $db =  \models\DB::getInstance();
        $set = array();
        $exec = array();
        foreach ($review as $key => $value) {
            $set[] = "$key=?";
            $exec[] = $value;
        }
        $sql = 'SELECT review_id FROM ' . $this->table . ' WHERE review_id = ? LIMIT 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($review['review_id']));
        $result = $up->fetchColumn(0);
        $up->closeCursor();
        if ($result) {
            $set[] = 'updated_time=NOW()';
            $exec[] = $review['review_id'];
            $sql = 'UPDATE ' . $this->table . ' SET ' . join(',', $set) . ' where review_id=?';
            $up = $db->prepare($sql);
            $r = $up->execute($exec); //update
            if ($r)
                return $review['review_id'];
        } else {
            $set[] = 'updated_time=NOW()';
            $sql = 'INSERT INTO ' . $this->table . ' SET ' . join(',', $set);
            $up = $db->prepare($sql);
            $r = $up->execute($exec); //insert
            if ($r)
                return $review['review_id'];
        }
        return '';
    }

    function complete_status_reviews($ids) {
        if (empty($ids))
            return;
        $db = \models\DB::getInstance();
        $ids = array_filter($ids);
        $sql = 'UPDATE ' . $this->table . ' SET status=?,updated_time=NOW() where review_id in (' . join(',', $ids) . ')';
        $up = $db->prepare($sql);
        $r = $up->execute(array('completed'));
    }

}
