<?php

namespace parsers;

class HostelworldParser extends AbstractHotelParser {

    var $table = 'hostelworld_reviews';

    function parse_hotel($url, $property_id) {
        require_once(dirname(__FILE__) . '/../../includes/simple_html_dom.php');
        $data = array();
        $ids = array();
        
        list($url,$t) = explode('?', $url); //skip ?
        $url.='?period=all';
        die($url);

        while (!empty($url)) {
            $page = $this->getCachedUrl($url);
            $html = str_get_html($page);

            $reviews = $html->find('.microreviews');
            if (!empty($reviews)) {
                foreach ($reviews as $item) {
                    $reviewData = array();

                    $reviewData['property_id'] = $property_id;
                    $reviewData['status'] = 'processing';

                    $commentBlock = $item->find('.translate', 0);
                    if (!is_null($commentBlock)) {
                        $reviewData['review_id'] = preg_replace('/[^\d]+/', '', $commentBlock->id);
                        $reviewData['text'] = trim($commentBlock->innertext);
                    } else {
                        continue;
                    }

                    $ratingBlock = $item->find('.ratingtd', 0);
                    $reviewData['percentage'] = (!is_null($ratingBlock) ? (int) $ratingBlock->innertext : '');

                    $dataBlock = $item->find('.reviewrating', 0);
                    $reviewData['date'] = (!is_null($dataBlock) ? date("Y-m-d", strtotime($dataBlock->innertext)) : '');


                    if ($this->check_on_stop_search($reviewData)) {
                        unset($html);
                        break 2;
                    }

                    $temp = $item->find('.reviewer li');
                    if (count($temp) > 1) {

                        $genderAgeBlock = $temp[count($temp) - 2];
                        if (!empty($genderAgeBlock->innertext)) {
                            $genderAgeText = explode(',', $genderAgeBlock->innertext);
                            $reviewData['gender'] = trim($genderAgeText[0]);
                            $reviewData['age'] = trim($genderAgeText[1]);
                        } else {
                            $reviewData['gender'] = '';
                            $reviewData['age'] = 0;
                        }
                    } else {
                        $reviewData['gender'] = $reviewData['age'] = '';
                    }
                    $ids[] = $this->insert_review_in_db($reviewData);
                }
                //
                $url = '';
                $nextUrlBlock = $html->find('.next', 0);

                if (!is_null($nextUrlBlock)) {
                    $url = $nextUrlBlock->href;
                }
            }
            unset($html);
        }
        $this->complete_status_reviews($ids);
        return count($ids);
    }

}
