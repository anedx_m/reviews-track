<?php

namespace stats;

class HtmlStatsAggregator {

    var $sites;
    var $type;
    var $stat_type;
    var $date_start;
    var $date_end;

    CONST DAILY = 1;
    CONST BI_MOTHNLY = 2;

    function __construct($date_start, $date_end, $id, $type = 'property', $stat_type = self::DAILY) {
        $this->type = $type;
        $this->stat_type = $stat_type;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->sites[] = new BookingStatsExtractor($date_start, $date_end, $id, $type);
        $this->sites[] = new HostelworldStatsExtractor($date_start, $date_end, $id, $type);
    }

    function create_top_stats_html() {

        foreach ($this->sites as $site) {
                $site->top_stats_data();
            //тут описываем разделитель для них в HTML;
        }
    }

    function create_middle_stats_html() {
        foreach ($this->sites as $site) {
                $site->middle_stats_data();
            //тут описываем разделитель для них в HTML;
        }
    }

    function create_bottom_stats_html() {

        switch ($this->type) {
            case 'property':
                foreach ($this->sites as $site) {
                    if (!empty($site->reviews))
                        $site->reviews_data();
                    //тут описываем разделитель для них в HTML;
                }
                break;

            case 'group':
                $ids = $this->sites[0]->get_ids();

                foreach ($ids as $id) {
                    ?><div style='border: 1px solid #000;'><?php //тут описываем разделитель для них в HTML;
                        $obj = new HtmlStatsAggregator($this->date_start, $this->date_end, $id, 'property', $this->stat_type);
                        echo $obj->create_html();
                        ?></div><?php
                }

                break;
            default:
                break;
        }
    }

    function create_html() {
        ob_start();
        $this->create_top_stats_html();
        ?><div></div><?php //тут описываем разделитель для них в HTML;
        if ($this->stat_type != self::DAILY)
            $this->create_middle_stats_html();
        $this->create_bottom_stats_html();
        $html = ob_get_clean();
        return $html;
    }

}
