<?php

namespace stats;

abstract class AbstractStatsExtractor {

    CONST MONTH = 1;
    CONST YEAR = 2;

    var $date_start;
    var $date_end;
    var $id;
    var $type;
    var $reviews;

    function __construct($date_start, $date_end, $id, $type = 'property') {
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->id = $id;
        $this->type = $type;
        $this->ids = $this->get_ids();
        $this->reviews=  $this->get_new_reviews();
    }

    function top_stats_data() {
        
    }

    function middle_stats_data() {
        
    }

    function reviews_data() {
        
    }

    function get_ids() {
        $db = \models\DB::getInstance();
        switch ($this->type) {
            case 'property':
                return array($this->id);
                break;
            case 'group':
                $sql = 'SELECT property_id FROM property_group WHERE group_id = ?';
                $up = $db->prepare($sql);
                $r = $up->execute(array($this->id));
                $result = $up->fetchAll(\PDO::FETCH_ASSOC);
                if ($result) {
                    $property_id = array();
                    foreach ($result as $value) {
                        $property_id[] = $value['property_id'];
                    }
                    return $property_id;
                } else
                    return array();
                break;
            default:
                break;
        }
    }

    function get_new_reviews() {
        $db = \models\DB::getInstance();

        $sql = 'SELECT COUNT(*) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ?';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT AVG(percentage) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ?';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

}
