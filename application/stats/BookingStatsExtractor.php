<?php

namespace stats;

class BookingStatsExtractor extends AbstractStatsExtractor {

    var $table = 'booking_reviews';

    function top_stats_data() {
        //var_dump($this->get_new_reviews());
        //die();
        $data_fields = array();
        $data_fields['Booking New Reviews'] = $this->get_new_reviews();
        $data_fields['Avg Booking Rating'] = $this->get_average_rating(self::MONTH);
        $data_fields['Avg Booking Rating Month'] = $this->get_average_rating(self::MONTH);
        $data_fields['Avg Booking Rating Year'] = $this->get_average_rating(self::YEAR);
        $data_fields['Avg 1 nights'] = $this->get_average_rating_by_nights(1);
        $data_fields['Avg 2 nights'] = $this->get_average_rating_by_nights(2);
        $data_fields['Avg 3 nights'] = $this->get_average_rating_by_nights(3, true);
        $data_fields['Popular Room Type'] = $this->get_popular_room_type();

        include APP_PATH . "/views/email/booking/top_stats.php";
    }

    function middle_stats_data() {

        $data_fields = array();
        $data_fields['Room Type With Highest Average Rating Month'] = $this->get_average_room_type_by_highest_rating(self::MONTH);
        $data_fields['Room Type With Lowest Average Rating Month'] = $this->get_average_room_type_by_low_rating(self::MONTH);


        $data_fields['Room Type With Highest Average Rating Year'] = $this->get_average_room_type_by_highest_rating(self::YEAR);
        $data_fields['Room Type With Lowest Average Rating Year'] = $this->get_average_room_type_by_low_rating(self::YEAR);

        $data_fields['Number Nights With Highest Average Rating Month'] = $this->get_average_nights_by_highest_rating(self::MONTH);
        $data_fields['Number Nights With Lowest Average Rating Month'] = $this->get_average_nights_by_low_rating(self::MONTH);


        $data_fields['Number Nights With Highest Average Rating Year'] = $this->get_average_nights_by_highest_rating(self::YEAR);
        $data_fields['Number Nights With Lowest Average Rating Year'] = $this->get_average_nights_by_low_rating(self::YEAR);

        include APP_PATH . "/views/email/booking/middle_stats.php";
    }

    function reviews_data() {

        $db = \models\DB::getInstance();
        $sql = 'SELECT percentage,nights,room,text FROM ' . $this->table . ' WHERE property_id = ? AND date >= ? AND date <= ? AND status <> ?';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->id, $this->date_start, $this->date_end, 'processing'));
        $reviews = $up->fetchAll(\PDO::FETCH_ASSOC);

        include APP_PATH . "/views/email/booking/reviews.php";
    }

    function get_average_rating_by_nights($nights, $more = false) {
        $db = \models\DB::getInstance();
        if ($more)
            $more = ">=";
        else
            $more = "=";

        $sql = 'SELECT AVG(percentage) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND nights ' . $more . ' ?';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing', $nights));
        $result = $up->fetchColumn(0);
        $up->closeCursor();


        return $result;
    }

    function get_popular_room_type() {
        $db = \models\DB::getInstance();

        $sql = 'SELECT room FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by room order by COUNT(*) DESC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_room_type_by_low_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT room FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by room order by AVG(percentage) ASC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_room_type_by_highest_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT room FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by room order by AVG(percentage) DESC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();


        return $result;
    }

    function get_average_nights_by_low_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT nights FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by nights order by AVG(percentage) ASC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_nights_by_highest_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT nights FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by nights order by AVG(percentage) DESC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

}
