<?php

namespace stats;

class HostelworldStatsExtractor extends AbstractStatsExtractor {

    var $table = 'hostelworld_reviews';

    function top_stats_data() {
        
        $data_fields = array();
        $data_fields['HostelWorld New Reviews'] = $this->get_new_reviews();
        $data_fields['Avg HostelWorld Rating'] = $this->get_average_rating(self::MONTH);
        $data_fields['Avg HostelWorld Rating Month'] = $this->get_average_rating(self::MONTH);
        $data_fields['Avg HostelWorld Rating Year'] = $this->get_average_rating(self::YEAR);
        $data_fields['Male Reviews'] = $this->get_new_reviews_male();
        $data_fields['Female Reviews'] = $this->get_new_reviews_female();
        $data_fields['Couple Reviews'] = $this->get_new_reviews_couple();

        include APP_PATH . "/views/email/hostelworld/top_stats.php";
    }

    function middle_stats_data() {

        $data_fields = array();
        $data_fields['Average Percent Female Reviews Month'] = $this->get_average_rating_by_female(self::MONTH);
        $data_fields['Average Percent Male Reviews Month'] = $this->get_average_rating_by_male(self::MONTH);
        $data_fields['Age Range With Lowest Average Rating Month'] = $this->get_average_age_range_by_low_rating(self::MONTH);
        $data_fields['Age Range With Highest Average Rating Month'] = $this->get_average_age_range_by_hight_rating(self::MONTH);

        $data_fields['Average Percent Female Reviews Year'] = $this->get_average_rating_by_female(self::YEAR);
        $data_fields['Average Percent Male Reviews Year'] = $this->get_average_rating_by_male(self::YEAR);
        $data_fields['Age Range With Lowest Average Rating Year'] = $this->get_average_age_range_by_low_rating(self::YEAR);
        $data_fields['Age Range With Highest Average Rating Year'] = $this->get_average_age_range_by_hight_rating(self::YEAR);

        include APP_PATH . "/views/email/hostelworld/middle_stats.php";
    }

    function reviews_data() {


        $db = \models\DB::getInstance();
        $sql = 'SELECT percentage,age,gender,text FROM ' . $this->table . ' WHERE property_id = ? AND date >= ? AND date <= ? AND status <> ?';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->id, $this->date_start, $this->date_end, 'processing'));
        $reviews = $up->fetchAll(\PDO::FETCH_ASSOC);

        include APP_PATH . "/views/email/hostelworld/reviews.php";
    }

    function get_new_reviews_male() {
        $db = \models\DB::getInstance();

        $sql = 'SELECT COUNT(*) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND (gender = ? OR gender = ?)';
        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing', 'Male', 'All Male Group'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_new_reviews_female() {
        $db = \models\DB::getInstance();

        $sql = 'SELECT COUNT(*) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND (gender = ? OR gender = ?)';

        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing', 'Female', 'All Female Group'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_new_reviews_couple() {
        $db = \models\DB::getInstance();

        $sql = 'SELECT COUNT(*) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND (gender = ? OR gender = ?)';

        $up = $db->prepare($sql);
        $r = $up->execute(array($this->date_start, $this->date_end, 'processing', 'Couple', 'Mixed Group'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_rating_by_female($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT AVG(percentage) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND (gender = ? OR gender = ?)';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing', 'Female', 'All Female Group'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_rating_by_male($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT AVG(percentage) FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? AND (gender = ? OR gender = ?)';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing', 'Male', 'All Male Group'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_age_range_by_low_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT age FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by age order by AVG(percentage) ASC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();

        return $result;
    }

    function get_average_age_range_by_hight_rating($my = self::MONTH) {
        $db = \models\DB::getInstance();
        if ($my == self::MONTH)
            $start_date = date('Y-m-01', strtotime($this->date_end));
        else
            $start_date = date('Y-01-01', strtotime($this->date_end));

        $sql = 'SELECT age FROM ' . $this->table . ' WHERE property_id in (' . join(',', $this->ids) . ') AND date >= ? AND date <= ? AND status <> ? group by age order by AVG(percentage) DESC limit 1';
        $up = $db->prepare($sql);
        $r = $up->execute(array($start_date, $this->date_end, 'processing'));
        $result = $up->fetchColumn(0);
        $up->closeCursor();
        return $result;
    }

}
