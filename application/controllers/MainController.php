<?php

namespace controllers;

use \models\Helper as Helper;
use \models\DB as DB;
use \PDO as PDO;

class MainController extends Controller {

	public $defaultAction = 'Entry';

	public function actionEntry() {
		$this->render( 'main', array() );
	}

	public function actionProperties() {
		$this->render( 'Properties', array() );
	}

	public function actionSaveProperty() {
		$property_id	 = Helper::getRequest( 'property_id' );
		$property_name	 = Helper::getRequest( 'property_name' );
		$hostelworld_url = Helper::getRequest( 'hostelworld_url' );
		$booking_url	 = Helper::getRequest( 'booking_url' );
		$db				 = DB::getInstance();
		if ( $property_id ) {
			$pr = $db->prepare( 'UPDATE properties SET name=:property_name, hostelworld_url=:hostelworld_url,booking_url=:booking_url WHERE id=:property_id' );
			$pr->execute( array( ':property_name' => $property_name, ':hostelworld_url' => $hostelworld_url, ':booking_url' => $booking_url, ':property_id' => $property_id ) );
		} else {
			$pr = $db->prepare( 'INSERT INTO properties (name, hostelworld_url,booking_url) VALUES (:property_name,:hostelworld_url,:booking_url)' );
			$pr->execute( array( ':property_name' => $property_name, ':hostelworld_url' => $hostelworld_url, ':booking_url' => $booking_url ) );
		}
	}

	public function actionDeleteProperty() {
		$property_id = Helper::getRequest( 'property_id' );
		if ( $property_id ) {
			$db	 = DB::getInstance();
			
			$pr	 = $db->prepare( 'DELETE FROM properties WHERE id=:property_id' );
			$pr->execute( array( ':property_id' => $property_id ) );
			
			$pr	 = $db->prepare( 'DELETE FROM property_group WHERE property_id=:property_id' );
			$pr->execute( array( ':property_id' => $property_id ) );
		}
	}

	public function actionListProperties() {
		$TProperties = new \tables\PropertiesTable();
		$TProperties->display();
	}

	public function actionGetProperty() {
		$property_id = Helper::getRequest( 'property_id' );
		$data		 = array();
		if ( $property_id ) {
			$db		 = DB::getInstance();
			$pr		 = $db->prepare( 'SELECT * FROM properties WHERE id=:property_id' );
			$r		 = $pr->execute( array( ':property_id' => $property_id ) );
			$data	 = $pr->fetch( PDO::FETCH_ASSOC );
		}
		Helper::jsonMsg( $data );
	}

	public function actionUsers() {
		$this->render( 'Users', array() );
	}

	public function actionSaveUser() {
		$user_id	 = Helper::getRequest( 'user_id' );
		$user_name	 = Helper::getRequest( 'user_name' );
		$user_email	 = Helper::getRequest( 'user_email' );
		$db			 = DB::getInstance();
		if ( $user_id ) {
			$pr = $db->prepare( 'UPDATE users SET name=:user_name, email=:user_email WHERE id=:user_id' );
			$pr->execute( array( ':user_name' => $user_name, ':user_email' => $user_email, ':user_id' => $user_id ) );
		} else {
			$pr = $db->prepare( 'INSERT INTO users (name, email) VALUES (:user_name,:user_email)' );
			$pr->execute( array( ':user_name' => $user_name, ':user_email' => $user_email ) );
		}
	}

	public function actionDeleteUser() {
		$user_id = Helper::getRequest( 'user_id' );
		if ( $user_id ) {
			$db	 = DB::getInstance();
			$pr	 = $db->prepare( 'DELETE FROM users WHERE id=:user_id' );
			$pr->execute( array( ':user_id' => $user_id ) );
			
			$pr	 = $db->prepare( 'DELETE FROM frequency_reports WHERE user_id=:user_id' );
			$pr->execute( array( ':user_id' => $user_id ) );
			
			$pr	 = $db->prepare( 'DELETE FROM property_group WHERE group_id IN (SELECT id FROM groups WHERE user_id=:user_id)' );
			$pr->execute( array( ':user_id' => $user_id ) );
			
			$pr	 = $db->prepare( 'DELETE FROM groups WHERE user_id=:user_id' );
			$pr->execute( array( ':user_id' => $user_id ) );
			
		}
	}

	public function actionListUsers() {
		$TUsers = new \tables\UsersTable();
		$TUsers->display();
	}

	public function actionGetUser() {
		$user_id = Helper::getRequest( 'user_id' );
		$data	 = array();
		if ( $user_id ) {
			$db		 = DB::getInstance();
			$pr		 = $db->prepare( 'SELECT * FROM users WHERE id=:user_id' );
			$r		 = $pr->execute( array( ':user_id' => $user_id ) );
			$data	 = $pr->fetch( PDO::FETCH_ASSOC );
		}
		Helper::jsonMsg( $data );
	}

	public function actionListUserGroups() {
		$TUserGroups = new \tables\UserGroupsTable();
		$TUserGroups->display();
	}

	public function actionEditGroup() {
		$user_id = Helper::getRequest( 'user_id' );
		$this->renderPartial( 'EditGroup', array( 'user_id' => $user_id ) );
	}

	public function actionGetGroup() {
		$group_name	 = Helper::getRequest( 'group_name' );
		$user_id	 = Helper::getRequest( 'user_id' );
		if ( !$group_name OR ! $user_id )
			return;

		$db		 = DB::getInstance();
		$pr		 = $db->prepare( 'SELECT * FROM groups WHERE name LIKE :group_name AND user_id=:user_id' );
		$r		 = $pr->execute( array( ':group_name' => $group_name, ':user_id' => $user_id ) );
		$data	 = $pr->fetch( PDO::FETCH_ASSOC );

		echo json_encode( $data );
	}

	public function actionDeleteGroup() {
		$group_id = Helper::getRequest( 'group_id' );

		if ( !$group_id )
			return;
		$db	 = DB::getInstance();
		$pr	 = $db->prepare( 'DELETE FROM property_group WHERE group_id=:group_id' );
		$pr->execute( array( ':group_id' => $group_id ) );
		$pr	 = $db->prepare( 'DELETE FROM groups WHERE id=:group_id' );
		$pr->execute( array( ':group_id' => $group_id ) );
	}

	public function actionGetGroups() {

		$group_name	 = Helper::getRequest( 'group_name' );
		$user_id	 = Helper::getRequest( 'user_id' );

		if ( !$user_id )
			return;

		$db		 = DB::getInstance();
		$pr		 = $db->prepare( 'SELECT name FROM groups WHERE name LIKE :group_name AND user_id=:user_id' );
		$r		 = $pr->execute( array( ':user_id' => $user_id, ':group_name' => $group_name . '%' ) );
		$data	 = $pr->fetchAll( PDO::FETCH_COLUMN );

		echo json_encode( $data );
	}

	public function actionSaveGroup() {
		$group_id		 = Helper::getRequest( 'group_id' );
		$group_name		 = Helper::getRequest( 'group_name' );
		$property_ids	 = Helper::getRequest( 'property_ids' );
		$user_id		 = Helper::getRequest( 'user_id' );
//print_r( $_REQUEST );
		if ( (!$group_id AND ! $group_name) OR ! $user_id )
			return;
		$db				 = DB::getInstance();

		if ( $group_id ) {
			$db->exec( 'DELETE FROM property_group WHERE group_id=' . $group_id );
		} else {
			$pr			 = $db->prepare( 'INSERT INTO groups (name,user_id) VALUES (:group_name,:user_id)' );
			$pr->execute( array( ':group_name' => $group_name, ':user_id' => $user_id ) );
			$group_id	 = $db->lastInsertId();
		}

		if ( !is_array( $property_ids ) )
			return;

		foreach ( $property_ids as $property_id ) {
			$pr = $db->prepare( 'INSERT INTO property_group (property_id, group_id) VALUES (:property_id,:group_id)' );
			$pr->execute( array( ':property_id' => $property_id, ':group_id' => $group_id ) );
		}
	}

	public function actionGetProperties() {

		$db				 = DB::getInstance();
		$property_name	 = Helper::getRequest( 'property' );
		$sql			 = 'SELECT id,name as text FROM properties WHERE name LIKE :property_name';
//		print_r( $sql );
		$pr				 = $db->prepare( $sql );
		$r				 = $pr->execute( array( ':property_name' => $property_name . '%' ) );
		$properties		 = $pr->fetchAll( PDO::FETCH_ASSOC );
		echo json_encode( $properties );
	}

	public function actionEditFrequencyReport() {
		$user_id = Helper::getRequest( 'user_id' );
		$this->renderPartial( 'EditFrequencyReports', array( 'user_id' => $user_id ) );
	}

	public function actionListFrequencyReports() {
		$TFrequencyReports = new \tables\FrequencyReportsTable();
		$TFrequencyReports->display();
	}

	public function actionDeleteFrequencyReport() {
		$frequency_report_id = Helper::getRequest( 'frequency_report_id' );

		if ( !$frequency_report_id )
			return;
		$db	 = DB::getInstance();
		$pr	 = $db->prepare( 'DELETE FROM frequency_reports WHERE id=:frequency_report_id' );
		$pr->execute( array( ':frequency_report_id' => $frequency_report_id ) );
		echo 'deleted';
	}

	public function actionSaveFrequencyReport() {
		$frequency_report_id = Helper::getRequest( 'frequency_report_id' );
		$frequency			 = Helper::getRequest( 'frequency' );
//print_r( $_REQUEST );
		if ( !$frequency_report_id OR ! $frequency )
			return;
		$db					 = DB::getInstance();
		$pr					 = $db->prepare( 'UPDATE frequency_reports SET frequency=:frequency WHERE id=:frequency_report_id' );
		print_r( 'UPDATE frequency_reports SET frequency=:frequency WHERE id=:frequency_report_id' );
		$pr->execute( array( ':frequency' => $frequency, ':frequency_report_id' => $frequency_report_id ) );
	}

	public function actionAddFrequencyReport() {

		$user_id	 = Helper::getRequest( 'user_id' );
		$entity_type = Helper::getRequest( 'entity_type' );
		$entity_id	 = Helper::getRequest( 'entity_id' );
		$frequency	 = Helper::getRequest( 'frequency_new' );
//print_r( $_REQUEST );
		if ( !$user_id OR ! $entity_type OR ! $entity_id )
			return;
		$db			 = DB::getInstance();
		$pr			 = $db->prepare( 'INSERT INTO frequency_reports (user_id, entity_id, entity_type, frequency) VALUES (:user_id,:entity_id,:entity_type,:frequency)' );
		$pr->execute( array( ':user_id' => $user_id, ':entity_id' => $entity_id, ':entity_type' => $entity_type, ':frequency' => $frequency ) );
	}

	public function actionGetEntityNames() {
		$user_id		 = Helper::getRequest( 'user_id' );
		$entity_name	 = Helper::getRequest( 'entity_name' );
		$user_id		 = (int) $user_id;
		$db				 = DB::getInstance();
		$where			 = "name LIKE '%$entity_name%'";
		$sql			 = "SELECT properties.id as entity_id,properties.name, 'property' as entity_type FROM properties WHERE $where
					UNION
					SELECT groups.id as entity_id,groups.name, 'group' as entity_type   FROM groups WHERE  groups.user_id=$user_id AND $where";
		//print_r( $sql );
		$pr				 = $db->prepare( $sql );
		$prepare_params	 = array();
		$r				 = $pr->execute( $prepare_params );
		$entities		 = $pr->fetchAll( PDO::FETCH_ASSOC );
		echo json_encode( $entities );
	}

}
