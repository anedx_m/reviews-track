<?php

namespace controllers;

/* Main controller class hosts common functions */

class Controller {

    public $defaultAction = 'default';
    public $defaultLayout = 'main';

    public function renderPartial($view, $param = array()) {
        if (!is_null($param)) {
            extract($param);
        }
        $view = basename($view);
        include APP_PATH . "/views/$view.php";
    }

    public function render($view, $param = array(), $layout = null) {
        if (!is_null($param)) {
            extract($param);
        }
        if (!$layout) {
            $layout = $this->defaultLayout;
        }
        include APP_PATH . "/views/layouts/{$layout}.php";
    }

    public function actionDefault() {
        echo 'default';
    }

}
