<?php

namespace models;

class Helper {

    const JSON_MSG_OK = 0;
    const JSON_MSG_ERROR = 1;

    public static function getPost($name_param, $default_value = '') {
        return isset($_REQUEST[$name_param]) ? $_REQUEST[$name_param] : $default_value;
    }

    public static function getRequest($name_param, $default_value = '') {
        return isset($_REQUEST[$name_param]) ? $_REQUEST[$name_param] : $default_value;
    }

    public static function getParam($name_param, $default_value = '') {
        return isset($_GET[$name_param]) ? $_GET[$name_param] : $default_value;
    }

    public static function getValue($arr, $name_param, $default_value = '') {
        return isset($arr[$name_param]) ? $arr[$name_param] : $default_value;
    }

    public static function jsonMsg($msg, $type = self::JSON_MSG_OK) {
        echo json_encode(array('msg' => $msg, 'type' => $type));
    }

    public static function getOption($option) {

        $sql = 'SELECT value FROM options WHERE `name`=:option';
        $query_get_option = $db->prepare($sql);

        $r = $query_get_option->execute(array(':option' => $option));
        $v = $query_get_option->fetch(\PDO::FETCH_COLUMN);
        $v = unserialize($v);
        $query_get_option->closeCursor();
        return $v;
    }

    public static function updateOption($option, $value) {

        $value = serialize($value);

        $db = \models\DB::getInstance();
        $op = self::getOption($option);
        if ($op) {
            $sql = 'UPDATE options SET value=:value WHERE `name`=:option';
            $up = $db->prepare($sql);
            $r = $up->execute(array(':option' => $option, ':value' => $value));
            return $r;
        } else {
            $sql = 'INSERT INTO options (`name`,value) VALUES (:option,:value)';
            $up = $db->prepare($sql);
            $r = $up->execute(array(':option' => $option, ':value' => $value));
            $up->closeCursor();
            return $r;
        }
    }

    public static function renderPartial($view, $param = array()) {
        if (!is_null($param)) {
            extract($param);
        }
        $view = basename($view);
        include APP_PATH . "/views/$view.php";
    }

    public static function getRenderPartial($view, $param = array()) {
        ob_start();
        if (!is_null($param)) {
            extract($param);
        }
        $view = basename($view);
        include APP_PATH . "/views/$view.php";
        $r = ob_get_clean();
        return $r;
    }

    public static function getValueByPropertyId($id, $value) {
        $db = \models\DB::getInstance();
        $sql = 'SELECT ' . $value . ' FROM properties WHERE id = ?';
        $up = $db->prepare($sql);
        $up->execute(array($id));
        $result = $up->fetchColumn(0);
        $up->closeCursor();
        return $result;
    }

    public static function getPropertiesUrls($value) {
        $db = \models\DB::getInstance();
        $sql = 'SELECT id,' . $value . ' as url FROM properties';
        $up = $db->prepare($sql);
        $up->execute(array());
        $urls = $up->fetchAll(\PDO::FETCH_ASSOC);

        return $urls;
    }

}
