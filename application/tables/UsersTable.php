<?php
namespace tables;

use \models\Helper as Helper;
use \models\DB as DB;
use \PDO as PDO;

class UsersTable extends Table {

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'actions':
				return '<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-default edit_user" href="#"><i class="fa fa-edit fa-fw"></i> Edit</a>
					<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-default assign_group" href="#"><i class="fa fa-group fa-fw"></i> Groups</a>
					<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-default frequency_reports" href="#"><i class="fa fa-calendar fa-fw"></i> Schedule</a>
					<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-danger delete_user" href="#"><i class="fa fa-trash-o fa-lg" ></i> Delete</a>
					';
			default:
				return $data_row[ $field ];
		}
	}

	public function getSortableColumns() {
		return array( 'name', 'email' );
	}

	public function getFields() {
		return array(
			'name'		 => 'Nickname',
			'email'		 => 'User email',
			'actions'	 => '' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function getData() {
		$db = \models\DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where = '';

		$prepare_params = array();

		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;

		$sc = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			$order_by_sql = "ORDER BY $order_by $direction";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';
		if ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT * FROM users $where $order_by_sql $limit_sql";
		
		$users	 = $db->prepare( $sql );
				
		$r		 = $users->execute( $prepare_params );
		$users	 = $users->fetchAll( \PDO::FETCH_NAMED );

		$amount			 = $db->prepare( 'SELECT COUNT(*) FROM users ' . $where );
		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );
		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( \PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $users;
	}

}
