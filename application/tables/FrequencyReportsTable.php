<?php

namespace tables;

use \models\Helper as Helper;
use \models\DB as DB;
use \PDO as PDO;

class FrequencyReportsTable extends Table {

    public $limit = 10;

    public function processingRow($data_row) {
        return '<tr data-id="' . $data_row['id'] . '">';
    }

    public function processingField($data_row, $field) {

        switch ($field) {
            case 'name':
                $pg = $data_row['entity_type'] == 'property' ? '' : '[G] ';
                return '<span class="' . $data_row['entity_type'] . '">' . $pg . $data_row['name'] . '</span>';

            case 'frequency':
                $frequency = $data_row['frequency'];
//				var_dump($frequency);
                $r = Helper::getRenderPartial('frequency', array('frequency' => $frequency, 'id' => $data_row['id']));
                return $r;
            case 'actions':
                return '<a data-type="' . $data_row['entity_type'] . '" data-id="' . $data_row['entity_id'] . '" class="btn btn-primary view_report" href="#"><i class="fa fa-search fa-lg" ></i> </a>'
                        . '&nbsp;<a data-id="' . $data_row['id'] . '" class="btn btn-danger delete_frequency_report" href="#"><i class="fa fa-trash-o fa-lg" ></i> </a>';
            default:
                return $data_row[$field];
        }
    }

    public function getSortableColumns() {
        return array('name');
    }

    public function getFields() {
        return array(
            'name' => 'Name',
            'frequency' => 'Frequency',
            'actions' => '');
    }

    public function display() {
        $this->max_visible_pages = 10;
        $this->createTable();
    }

    public function getData() {
        $db = DB::getInstance();

        $current_page = $this->getCurrentPage();


        $prepare_params = array();
        $params = Helper::getRequest('params');
        $user_id = Helper::getValue($params, 'user_id');
        $where = '';
        $search = Helper::getValue($params, 'search');
        if ($search) {
            $where = "WHERE name LIKE :search";
            $prepare_params[':search'] = "%$search%";
        }
//		var_dump($_REQUEST);
//		die();
        if (!$user_id) {
            $this->amount = 0;
            return array();
        }


        $user_id = (int) $user_id;
//		$prepare_params[ ':user_id' ] = $user_id;

        $order_by = $this->order_by;
        $order_by_sql = '';
        $direction = $this->direction;

        $sc = $this->getSortableColumns();
        if ($order_by AND in_array($order_by, $sc)) {
            $order_by_sql = "ORDER BY $order_by $direction";
        }

        $limit = $this->limit;
        $limit_sql = '';
        if ($limit) {
            $limit_sql = "LIMIT :limit OFFSET :offset";
            $prepare_params[':limit'] = $limit;
            $prepare_params[':offset'] = ($current_page - 1) * $limit;
        }
        $sql = "SELECT * FROM (SELECT `frequency_reports`.*,properties.name FROM `frequency_reports` 
						INNER JOIN properties ON properties.id=frequency_reports.`entity_id`  
						WHERE  frequency_reports.user_id=$user_id AND frequency_reports.entity_type='property'
					UNION
					SELECT `frequency_reports`.*,groups.name FROM `frequency_reports` 
						INNER JOIN groups ON groups.id=frequency_reports.`entity_id` 
						WHERE  frequency_reports.user_id=$user_id AND frequency_reports.entity_type='group') AS F_R  $where  $order_by_sql $limit_sql";
//		print_r( $sql );
//		var_dump( $prepare_params );
        $frequency_reposts = $db->prepare($sql);
        $r = $frequency_reposts->execute($prepare_params);
        $frequency_reposts = $frequency_reposts->fetchAll(PDO::FETCH_NAMED);

        $amount = $db->prepare("SELECT COUNT(*) FROM (SELECT `frequency_reports`.*,properties.name FROM `frequency_reports` 
						INNER JOIN properties ON properties.id=frequency_reports.`entity_id`  
						WHERE  frequency_reports.user_id=$user_id AND frequency_reports.entity_type='property'
					UNION
					SELECT `frequency_reports`.*,groups.name FROM `frequency_reports` 
						INNER JOIN groups ON groups.id=frequency_reports.`entity_id` 
						WHERE  frequency_reports.user_id=$user_id AND frequency_reports.entity_type='group') AS F_R  $where");

        unset($prepare_params[':limit']);
        unset($prepare_params[':offset']);

        $r = $amount->execute($prepare_params);
        $amount = $amount->fetch(PDO::FETCH_COLUMN);
        //-=-=-=-=-=-=-=-=-
        $this->amount = $amount;
        //-=-=-=-=-=-=-=-=-
//		var_dump($frequency_reposts);
        return $frequency_reposts;
    }

}
