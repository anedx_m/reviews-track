<?php
namespace tables;

use \models\Helper as Helper;
use \models\DB as DB;
use \PDO as PDO;

class UserGroupsTable extends Table {

	public $limit = 10;

	public function processingRow( $data_row ) {
		return '<tr data-id="' . $data_row[ 'id' ] . '">';
	}

	public function processingField( $data_row, $field ) {

		switch ( $field ) {
			case 'properties':
				$group_id	 = $data_row[ 'id' ];
				$db			 = DB::getInstance();
				$r			 = $db->query( 'SELECT property_group.property_id as id,properties.name FROM property_group INNER JOIN properties ON property_group.property_id=properties.id WHERE group_id=' . $group_id );
				$properties	 = $r->fetchAll( PDO::FETCH_ASSOC );
				$select2	 = Helper::getRenderPartial( 'select2', array( 'group_id' => $group_id, 'properties' => $properties ) );
				return $select2;
			case 'actions':
				return '<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-danger delete_group" href="#"><i class="fa fa-trash-o fa-lg" ></i> </a>';
			default:
				return $data_row[ $field ];
		}
	}

	public function getSortableColumns() {
		return array( 'name' );
	}

	public function getFields() {
		return array(
			'name'		 => 'Group',
			'properties' => 'Properties',
			'actions'	 => '' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();


		$prepare_params	 = array();
		$params			 = Helper::getRequest( 'params' );
		$user_id		 = Helper::getValue( $params, 'user_id' );
//		var_dump($_REQUEST);
//		die();
		if ( !$user_id ) {
			$this->amount = 0;
			return array();
		}

		$where = 'WHERE user_id=:user_id';

		$prepare_params[ ':user_id' ] = $user_id;

		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;

		$sc = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			$order_by_sql = "ORDER BY $order_by $direction";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';
		if ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT groups.* FROM groups INNER JOIN users ON users.id=groups.user_id $where  $order_by_sql $limit_sql";
//		var_dump( $sql );
		$users	 = $db->prepare( $sql );
		$r		 = $users->execute( $prepare_params );
		$users	 = $users->fetchAll( PDO::FETCH_NAMED );

		$amount = $db->prepare( 'SELECT COUNT(*) FROM groups INNER JOIN users ON users.id=groups.user_id  ' . $where );

		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );

		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $users;
	}

}
