<?php
namespace tables;

use \models\Helper as Helper;
use \models\DB as DB;
use \PDO as PDO;

class PropertiesTable extends Table {

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'hostelworld_url':
				return '<a href="' . $data_row[ $field ] . '" target=_blank>' . $data_row[ $field ] . '</a>';
			case 'booking_url':
				return '<a href="' . $data_row[ $field ] . '" target=_blank>' . $data_row[ $field ] . '</a>';
			case 'actions':
				return '<a data-id="' . $data_row[ 'id' ] . '"class="btn btn-default edit_property" href="#"><i class="fa fa-edit fa-fw"></i> Edit</a>
                                            <a data-id="' . $data_row[ 'id' ] . '" class="btn btn-primary parse_property" href="#"><i class="fa fa-binoculars fa-sm" ></i> Parse Property</a>
						<a data-id="' . $data_row[ 'id' ] . '" class="btn btn-danger delete_property" href="#"><i class="fa fa-trash-o fa-lg" ></i> Delete</a>';
			default:
				return $data_row[ $field ];
		}
	}

	public function getSortableColumns() {
		return array( 'name', 'hostelworld_url', 'booking_url' );
	}

	public function getFields() {
		return array(
			'name'				 => 'Name',
			'hostelworld_url'	 => 'Hostelworld rewiew link',
			'booking_url'		 => 'Booking rewiew link',
			'actions'			 => '' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where			 = '';
		$params			 = Helper::getRequest( 'params' );
//		$search			 = Helper::getValue( $params, 'search' );
		$prepare_params	 = array();
//		if ( $search ) {
//			$where						 = "WHERE title LIKE :search";
//			$prepare_params[ ':search' ] = "%$search%";
//		}
		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;

		$sc = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			$order_by_sql = "ORDER BY $order_by $direction";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';
		if ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql		 = "SELECT * FROM properties $where $order_by_sql $limit_sql";
//		var_dump( $sql );
		$properties	 = $db->prepare( $sql );
		$r			 = $properties->execute( $prepare_params );
		$properties	 = $properties->fetchAll( PDO::FETCH_NAMED );

		$amount			 = $db->prepare( 'SELECT COUNT(*) FROM properties ' . $where );
		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );
		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $properties;
	}

}
