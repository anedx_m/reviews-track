<?php

include 'config.php';

function autoloader($class) {
    $path = dirname(__FILE__) . '/application/';
    $file = $path . $class . '.php';
    $file = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $file);
    
    if (file_exists($file)) {
        include $file;
    }
}

spl_autoload_register('autoloader');


