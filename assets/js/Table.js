function Table( url, target ) {
    var that = this
    this.url = url;
    this.target = target;
//					var order_by = $
    this.afterList = $.Callbacks();

    this.list = function( params, start_search ) {
        var order_by = $( that.target ).find( '.order_by' ).val()
        var direction = $( that.target ).find( '.direction' ).val()
        var current_page = $( that.target ).find( '.current-page' ).attr( 'data-page' );
//        console.log( params )
        if ( typeof ( params ) !== 'undefined' ) {
            if ( typeof ( params.order_by ) !== 'undefined' ) {
                order_by = params.order_by
                delete params.order_by;
            }
            if ( typeof ( params.direction ) !== 'undefined' ) {
                direction = params.direction
                delete params.direction;
            }
        }
        if ( typeof ( this.params ) == 'undefined' ) {
            this.params = { }
        }
        this.params.order_by = order_by;
        this.params.direction = direction;
        this.params.current_page = current_page;
        if ( typeof ( params ) !== 'undefined' ) {
            this.params.params = params
        }

        if ( start_search == 1 ) {
            this.params.start_search = 1
        } else {
            this.params.start_search = 0
        }
        $.post( this.url, this.params, function( data ) {
            $( that.target ).html( data )
            that.params.start_search = 0;
            that.afterList.fire()
        } )

    }
    this.afterList.add( function() {
        $( that.target ).find( 'th.sortable ' ).click( function() {
            var order_by = $( this ).attr( 'data-order_by' )
            var direction = $( that.target ).find( '.direction' ).val();

            if ( direction == 'ASC' ) {
                direction = 'DESC'
            } else {
                direction = 'ASC'
            }
            $( that.target ).find( '.order_by' ).val( order_by )
            $( that.target ).find( '.direction' ).val( direction )
            that.list(undefined,1)
        } )

        $( that.target ).find( '.number-page' ).click( function() {
            var page = $( this ).attr( 'data-page' );
            that.params.current_page = page;

            $.post( that.url, that.params, function( data ) {
                $( that.target ).html( data )
                that.afterList.fire()
            } )
        } )
    } )


}