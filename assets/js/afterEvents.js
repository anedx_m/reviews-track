( function( $ ) {

    $.fn.afterKeyDown = function( callback, msec, clearT ) {
        var clearT = isUndefined( clearT ) ? false : clearT;
        var that = this;
        that._t = ''
        $( this ).keydown( function( ev ) {
            if ( clearT )
                clearTimeout( that._t );
            that._t = setTimeout( callback, msec, ev )
        } )
    }
    $.fn.afterChange = function( callback, msec, clearT ) {
        var clearT = isUndefined( clearT ) ? false : clearT;
        var that = this;
        that._t = ''
        $( this ).change( function( ev ) {
            if ( clearT )
                clearTimeout( that._t );
            that._t = setTimeout( callback, msec, ev )
        } )
    }
    $.fn.afterKeyPress = function( callback, msec, clearT ) {
        var clearT = isUndefined( clearT ) ? false : clearT;
        var that = this;
        that._t = ''
        $( this ).keypress( function( ev ) {
            if ( clearT )
                clearTimeout( that._t );
            that._t = setTimeout( callback, msec, ev )
        } )
    }
    function getValue( arr, val, default_val ) {

        if ( isUndefined( arr ) || isUndefined( arr[val] ) ) {
//            if ( isUndefined( default_val ) ) {
//                return null
//            }
            return default_val
        } else {
            return arr[val]
        }
    }
    function isUndefined( val ) {
        if ( typeof ( val ) === "undefined" ) {
            return true
        } else {
            return false
        }
    }
} )( jQuery );