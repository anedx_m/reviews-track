<?php

include dirname(__FILE__).'/../init.php';

$hostelworld = new \parsers\HostelworldParser();
$time_start = microtime(true);
$urls_hostelworld = \models\Helper::getPropertiesUrls('hostelworld_url');

$count = 0;
foreach ($urls_hostelworld as $property_item) {
    $count += $hostelworld->parse_hotel($property_item['url'], $property_item['id']);
}

$time_end = microtime(true);

$execution_time = ($time_end - $time_start);

echo 'Added or Updated ' . $count . ' reviews. <b>Total Execution Time:</b> ' . $execution_time . ' Sec';
