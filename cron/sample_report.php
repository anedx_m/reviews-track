<?php

include '../init.php';
$stat_type = $_REQUEST['stat_type'];
$type = $_REQUEST['type'];
$id = $_REQUEST['id'];

$stat_type = $stat_type == 3 ? 2 : $stat_type;

if ($_REQUEST['stat_type'] == 1) {
    $now = date("Y-m-d");
    $creator = new \stats\HtmlStatsAggregator($now, $now, $id, $type, $stat_type);
    echo $creator->create_html();
} else {
    $day = date("m");
    if ($day > 15) {
        $start = date("Y-m-01");
        $end = date("Y-m-14");
    } else {
        $start = date("Y-m-d", strtotime('first day of previous month'));
        $end = date("Y-m-d", strtotime('last day of previous month'));
    }
    $creator = new \stats\HtmlStatsAggregator($start, $end, $id, $type, $stat_type);
    echo $creator->create_html();
}


