<?php

include dirname(__FILE__).'/../init.php';
set_time_limit(0);
ini_set('memory_limit','512M');
$_REQUEST['id'] =1;

$time_start = microtime(true);
$url_hostelworld = \models\Helper::getValueByPropertyId($_REQUEST['id'], 'hostelworld_url');
$url_booking = \models\Helper::getValueByPropertyId($_REQUEST['id'], 'booking_url');

$booking = new \parsers\BookingParser();
$hostelworld = new \parsers\HostelworldParser();


$count_b = $booking->parse_hotel($url_booking, $_REQUEST['id']);
$count_h = $hostelworld->parse_hotel($url_hostelworld, $_REQUEST['id']);
$time_end = microtime(true);

$execution_time = ($time_end - $time_start);

echo '<b>Hostelworld:</b> ' . $count_h . '<br>';
echo '<b>Booking:</b> ' . $count_b . '<br>';
echo '<b>Total Execution Time:</b> ' . $execution_time . ' Sec';
