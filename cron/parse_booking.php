<?php

include dirname(__FILE__).'/../init.php';

$booking = new \parsers\BookingParser();
$time_start = microtime(true);

$urls_booking = \models\Helper::getPropertiesUrls('booking_url');
$count = 0;
foreach ($urls_booking as $property_item) {
    $count += $booking->parse_hotel($property_item['url'], $property_item['id']);
}


$time_end = microtime(true);

$execution_time = ($time_end - $time_start);

echo 'Added or Updated ' . $count . ' reviews. <b>Total Execution Time:</b> ' . $execution_time . ' Sec';

